#ifndef __BROWSER_CONFIG_H__
#define __BROWSER_CONFIG_H__

#include <QtCore/QUrl>

namespace ZKJC {
namespace TouchScreen {
namespace Browser {

class Config {
    const QUrl &getHomeUrl() const { return _homeUrl; }

    static Config &instance() {
        static Config config;
        return config;
    }

private:
    Config();
    Config(Config &other);
    QUrl _homeUrl;
};

}
}
}

#endif
