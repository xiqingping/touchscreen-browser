#include <cstdlib>
#include <QtCore/QSettings>
#include "browser-config.h"

namespace ZKJC {
namespace TouchScreen {
namespace Browser {

Config::Config()
{
    QString configFilePath(::getenv("HOME"));
    configFilePath += "/.confg/zkjc/touchscreen/browser/config.ini";
    QSettings s(configFilePath, QSettings::IniFormat);
}

}
}
}
