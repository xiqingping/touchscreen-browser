#include <csignal>
#include <QtGui/QApplication>
#include <QtWebKit/QWebView>
#include <QtWebKit/QWebSettings>

void terminateApplication(int sig)
{
    qApp->quit();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    ::signal(SIGTERM, terminateApplication);
    ::signal(SIGINT, terminateApplication);


    QWebSettings *globalSettings = QWebSettings::globalSettings();
    globalSettings->setAttribute(QWebSettings::PluginsEnabled, true);
    globalSettings->setAttribute(QWebSettings::JavascriptEnabled, true);
    globalSettings->setAttribute(QWebSettings::JavascriptCanOpenWindows, true);
    globalSettings->setAttribute(QWebSettings::JavascriptCanCloseWindows, true);

    QWebView view;
    view.setUrl(QUrl("http://218.22.11.60:4444"));
    view.show();

    return app.exec();
}
